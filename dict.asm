%include "lib.inc"
%include "colon.inc"

global find_word


section .text

; Принимает указатель на нуль-терминированную строку и указатель на начало словаря.
; Возвращает адрес начала вхождения в словарь, если элемент найден, иначе 0
find_word:
	; rdi - char* string to check
	; rsi - pointer to linked list
.loop:					;
	cmp	rsi, LIST_NULL		; if next entry is null -> 
	je	.not_found		; -> string not found
	push	rdi			; save useful regs
	push	rsi			;
	add	rsi, KEY_OFFSET		; rsi <- char* (key pointer)
	call	string_equals		; check if key is equal to string we try to find
	pop	rsi			; load useful regs
	pop	rdi			;
	cmp	rax, 1			; if strings are equal ->
	je	.found			; -> we found it
	mov	rsi, [rsi]		; rsi <- next entry
	jmp	.loop			; 
.found:					;
	mov	rax, rsi		; rax <- entry pointer (found)
	ret				;
.not_found:				;
	xor	rax, rax		; rax <- 0 (not found)
	ret				;
