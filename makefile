ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.PHONY: run clean recoursion

main: main.o lib.o dict.o
	$(LD) -o $@ $^

dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm colon.inc words.inc lib.inc dict.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

run: main
	./main

clean:
	$(RM) *.o main

# haha "make recoursion" makes recoursion
recoursion:
	make recoursion
