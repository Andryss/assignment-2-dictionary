%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

global _start


section .rodata

hello_message:
	db "Hello. Type the query and I will try to find this key in my dict:", 0 
buffer_overflow_message:
	db "Sorry, dude, your search query is too long. Try a shorter query.", 0
word_found_message:
	db "Your query successfully completed. The found value is:", 0
word_not_found_message:
	db "Your query successfully completed. No value with current key found.", 0


section .bss

buffer: 
	resb BUFFER_SIZE


section .text

; Читает строку размером не более 255 символов в буфер с stdin
; Пытается найти вхождение в словаре
; если оно найдено, распечатывает в stdout значение по этому ключу
; Иначе выдает сообщение об ошибке
; Не забудьте, что сообщение об ошибках нужно выводить в stderr
_start:
	mov	rdi, hello_message		; greet user
	call	println_string			; (user friendly + 1)
						;
	mov	rdi, buffer			; read key from user
	mov	rsi, BUFFER_SIZE		; rax <- buffer with read word
	call	read_line			; rdx <- read line length (because read_word sucks)
						;
	test	rax, rax			; if some error occured ->
	jz	.buffer_overflow		; -> print error and exit
						;
	push	rdx				; save read word length
						;
	mov	rdi, rax			; try to find read word in dict
	get_list_into	rsi			; rsi <- found entry pointer
	call	find_word			;
						;
	pop	rdx				; load read word length
						;
	test	rax, rax			; if rax == 0 ->
	jz	.word_not_found			; -> word not found
						;
.word_found:					; 
	push	rax				; save entry pointer
	push	rdx				; save key length
	mov	rdi, word_found_message		; print word-found phrase
	call	println_string			;
	pop	rdx				; load saved regs
	pop	rdi				;
						;
	add	rdi, KEY_OFFSET			; calculate current entry key pointer
	add	rdi, rdx			; rdi <- key pointer + key_length
	inc	rdi
	call	println_string			; print value
	call	exit_ok				; exit with code OK
						;
.word_not_found:				;
	mov	rdi, word_not_found_message	; print word-not-found phrase
	call	println_string			;
	call	exit_ok				; exit with code OK
						;
.buffer_overflow:				;
	mov	rdi, buffer_overflow_message	; print buffer-overflow phrase in stderr
	call	print_error			;
	call	print_newline			;
	call	exit_err			; and exit with code ERROR
