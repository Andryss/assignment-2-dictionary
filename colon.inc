%define LIST_NULL 0	; Value of null pointer to the next entry
%define KEY_OFFSET 8	; Offset of a key in entry

%define LIST_POINTER LIST_NULL	; Pointer to the start of the list


; There is the macro for adding element in the dictionary

%macro colon 2					; define "colon" macro with 2 args
	%ifid %2				; if 2nd arg is id
		%2: dq LIST_POINTER		; create new entry
		%define LIST_POINTER %2		; redefine list pointer
	%else					; 
		%fatal "value must be id"	; else fatal (because "error" sucks)
	%endif					;
	%ifstr %1				; if 1st arg is str
		db %1, 0			; create entry's key
	%else					;
		%fatal "value must be string"	; else fatal
	%endif					;
%endmacro					;


; There is the macro for getting the list pointer

%macro get_list_into 1				; define "get_list_into" macro with 1 arg
	%ifid %1				; if 1st arg is id
		mov	%1, LIST_POINTER	; move LIST_POINTER to it
	%else					;
		%fatal "value must be id"	; else fatal
	%endif					;
%endmacro					;
